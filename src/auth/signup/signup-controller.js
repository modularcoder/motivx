motivX.auth

.controller("SignupCtrl", SignupCtrl);

function SignupCtrl($log, $state, Config, webAPIAuth, authService) {
	var vm = this;

	vm.user = {};
	vm.user.newsletter = true;
	vm.registerError = false;


	// Registering user
	vm.signup = function() {
		$log.log(vm.user);
      	vm.user.userName = vm.user.email;

		authService.register(vm.user)
		.then(function () {
			return webAPIAuth.login('password', vm.user.email, vm.user.password);
		})
		.then(function () {
			$state.go(Config.APP.defaultState);
		})
		.catch(function (error) {
			vm.registerError = true;
			vm.errormsg = error.data.message
		});
	}
}
