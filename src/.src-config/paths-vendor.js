var config = require("../../config.js");

// Vendor scripts processing
// Each object represents a bunch of files needs to be processed in certain way
exports.scripts = [
	// 1. Take script files installed by bower
	// 2. Concatenate them into venodr.js file
	// 3. Copy vendor.js virtual file to "js" in build directory
	{
		src: [
			config.bower_dir + "/angular/angular.js",
			config.bower_dir + "/ngstorage/ngStorage.js",
			config.bower_dir + "/angular-ui-router/release/angular-ui-router.js",
			config.bower_dir + "/angular-web-api2-auth/angular-web-api2-auth.src.js",
			config.bower_dir + "/angular-loading-bar/build/loading-bar.js",
		],
		tasks: [
			{
				name: "concat",
				options: "vendor.js"
			}
		],
		dest: config.build_dir + "/js"
	}
];

// Vendor styles processing
// We want to take bootstrap's files, nvd3 chart files, installed by bower
// And concatenate them into vendor.css which will be loaded from our layout files
exports.styles = [
	// 1. Take style files installed by bower
	// 2. Concatenate them into venodr.css file
	// 3. Copy vendor.css virtual file to "css" in build directory
	{
		src: [
			config.src_dir + "/_vendor/vendor.less"
		],
		tasks: [
			{
				name: "less",
			}
		],
		dest: config.build_dir + "/css"
	}
];

// Vendor assets processing
// Fonts, images, etc...
// Fonts, images, etc...
exports.assets = [
	// 1. Take font from font-awesome installed by bower
	// 2. Copy them to "fonts" in build directory
	// {
	// 	src: config.bower_dir + "/katachiWebfont/**/*",
	// 	dest: config.build_dir + "/fonts"
	// }
];