motivX.auth

.controller("LogoutCtrl", LogoutCtrl);

function LogoutCtrl($state, $log, webAPIAuth) {
	var vm = this;

	webAPIAuth.logout();

	$state.go('auth.login');
}
