/************************************************************
*						Project Configs
************************************************************/

	var config = require("./config");
	var paths = {};

	paths.app = require("./" + config.src_dir +"/.src-config/paths-app");
	paths.vendor = require("./" + config.src_dir +"/.src-config/paths-vendor");


/************************************************************
*					Libraries
************************************************************/
	
	var _ = require('lodash'),
		path = require('path'),
		glob = require('glob'),
		Combine = require('stream-combiner');

/************************************************************
*					Gulp Modules
************************************************************/
	
	// Common modules
	var gulp = require('gulp'),
		gutil = require('gulp-util'),
		connect = require('gulp-connect');


/************************************************************
*					Loading tasks
************************************************************/

	var tasks = {};

	// load all modules dynamically
	glob.sync(__dirname + '/tasks/*.js', {cwd: "/tasks"}).forEach(function(filePath) {

		var fileName = path.basename(filePath, '.js');

		tasks[fileName] = require(filePath);
	});


/************************************************************
*						Scripts
************************************************************/

	// // Handles application and vendor scripts
	gulp.task('scripts', ['scripts_app', 'scripts_vendor']);

	// Process application scripts
	gulp.task('scripts_app', function() {

		for(var i in paths.app.scripts) {

			processFileBunch({
				bunch: paths.app.scripts[i],
				reload: true
			});

		}

	});

	// Process vendor scripts
	gulp.task('scripts_vendor', function() {

		for(var i in paths.vendor.scripts) {

			processFileBunch({
				bunch: paths.vendor.scripts[i]
			});

		}

	});


/************************************************************
*						Styles
************************************************************/
	
	gulp.task('styles', ['styles_app', 'styles_vendor']);

	// Process application styles
	gulp.task('styles_app', function() {

		for(var i in paths.app.styles) {

			processFileBunch({
				bunch: paths.app.styles[i],
				reload: true
			});

		}
	});

	// Process vendor styles
	gulp.task('styles_vendor', function() {

		for(var i in paths.vendor.styles) {

			processFileBunch({
				bunch: paths.vendor.styles[i]
			});

		}

	});

/************************************************************
*						Assets
************************************************************/
	
	gulp.task('assets', ['assets_app', 'assets_vendor']);

	// Process vendor assets
	gulp.task('assets_vendor', function() {


		for(var i in paths.vendor.assets) {
			processFileBunch({
				bunch: paths.vendor.assets[i]
			});
		}

	});

	// Process application assets
	gulp.task('assets_app', function() {

		for(var i in paths.app.assets) {

			processFileBunch({
				bunch: paths.app.assets[i],
				reload: true
			});

		}

	});


/************************************************************
*						Layouts
************************************************************/
	
	// Process apllication layouts
	gulp.task('layouts', function() {

		for(var i in paths.app.layouts) {

			processFileBunch({
				bunch: paths.app.layouts[i],
				reload: true
			});

		}
		  	
	});

/************************************************************
*						Templates
************************************************************/

	// Process application templates
	gulp.task('templates', function() {

		for(var i in paths.app.templates) {

			processFileBunch({
				bunch: paths.app.templates[i],
				reload: true
			});

	  	}
	  
	});

/************************************************************
*						Other
************************************************************/

	// // Local server pointing on public folder
	gulp.task('connect', function() {
		connect.server({
			root: config.build_dir,
			port: 3333,
			livereload: true
		});
	});


	// Rerun the task when a file changes
	gulp.task('watch', function() {

		// When application script file changes, process application scripts
		for(var i in paths.app.scripts) {
			var watchPaths = paths.app.scripts[i].watch || paths.app.scripts[i].src;
			gulp.watch(watchPaths, ['scripts_app']);
		}

		// When application style changes, process application styles
		for(var i in paths.app.styles) {
			var watchPaths = paths.app.styles[i].watch || paths.app.styles[i].src;
			gulp.watch(watchPaths, ['styles_app']);
		}

		// When application layout changes, process application layouts
		for(var i in paths.app.layouts) {
			var watchPaths = paths.app.layouts[i].watch || paths.app.layouts[i].src;
			gulp.watch(watchPaths, ['layouts']);
		}

		// When application template changes, process application templates
		for(var i in paths.app.templates) {
			var watchPaths = paths.app.templates[i].watch || paths.app.templates[i].src;
			gulp.watch(watchPaths, ['templates']);
		}

		// When application asset changes, process application assets
		for(var i in paths.app.assets) {
			var watchPaths = paths.app.assets[i].watch || paths.app.assets[i].src;
			gulp.watch(watchPaths, ['assets_app']);
		}
		
	});



/************************************************************
*					
*************************************************************/


function processFileBunch (options) {


	var bunch =  options.bunch;
	var livereload = options.reload || false;


	// If item.src is array
	// Always ignore this files
	if(Array.isArray(bunch.src)) {
		bunch.src.push("!" + config.src_dir +"/paths-app.js");
		bunch.src.push("!" + config.src_dir +"/paths-vendor.js");	
	}

	var streams = [];

	if(typeof bunch.tasks !== "undefined") {
		bunch.tasks.map(function(task) {
			if(typeof tasks[task.name] !== "undefined") {

				var taskRunner = tasks[task.name].runner;

				var taskDefaults = tasks[task.name].defaults || null;
				var taskOptions = task.options || null;

				var options;

				if(_.isObject(taskOptions) && _.isObject(taskDefaults)) {
					options = _.assign(taskDefaults, taskOptions);
				}
				else {
					options = taskOptions || taskDefaults;
				}

				streams.push(taskRunner(options));
			}
			else {
				console.log("Task \"" + task.name + "\" is not defined...");
			}			
		});
	}

	// Assuming is always dest...
	streams.push(gulp.dest(bunch.dest));

	// If we should livereload
	if(livereload) {
		streams.push(connect.reload());
	}


	// Generate stream
	var generatedStream = Combine(streams);
	
	gulp.src(bunch.src)
		.pipe(generatedStream)
		.on('error', gutil.log);
		// .pipe(gulp.dest(item.dest));
		// .pipe(connect.reload());

}


/************************************************************
*					Global tasks
*************************************************************/
	
	// Builds the application
	// Run "gulp build --production" for production build 
	// with minified styles and scripts
	gulp.task('build', [
		'scripts', 
		'styles', 
		'layouts',
		'templates',
		'assets'
	]);

	// // Run this task for development
	gulp.task('develop', [
		'build',
		'watch', 
		'connect'
	]);

	gulp.task('default', ['develop']);