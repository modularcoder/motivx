motivX.auth

.factory('authSocialButtonsService', function($log, $http, Config) {

	return {
		get: function () {

			return $http.get(Config.API.url + "/api/account/ExternalLogins?generateState=true&returnUrl=/externaltoken")

			.then(function (response) {

				// Returning processed providers

				return response.data.map(function(provider) {

					// Prepending API base url
					provider.url = Config.API.baseUrl + "/" + provider.url.slice(1);

					return provider;
				});

			});
			
		}
	}
});
