Modularus
=========
A boilerplate for modular front-end development.

![modularus!](http://modularus.github.io/assets/images/logo.png)

## Introduction

Modularus is a set of tools and predefined file structure which allows you to create well-organized, easy scalable and maintainable front-end applications or wireframes.


## Supported configurations

Modularus supports some common configurations out of the box. 

* Frameworks: `AngularJS` @ToDo: `BackboneJS`, `EmberJS`
* Scripts: `JavaScript` @ToDo: `CoffeeScript`
* Markup:  `HTML` @ToDo: `EJS`, `Jade`
* Stylesheets: `CSS`, `LESS` @ToDo: `SASS`

But it doesn't limit or force to use any of those. If you want to use configurations which are different from listed ones, you should just little bit tweak the modularus for your own needs.


## Getting started

Modularus runs on NodeJs, so please make sure you have NodeJS and npm installed.

1. Install **[gulp](http://gulpjs.com/)** and **[bower](http://bower.io/)** globally `npm install -g gulp bower`
2. Install **npm** dependencies `npm install`
3. Install **bower** dependencies `bower install`
4. Build and launch the application `gulp`. This command will build the project in "public" folder and run a local web server.

**WARNING!** Never edit files in "public" folder because they are regenerated automatically every time and all your changes will be lost! **Always** work in `src/` folder!

## Examples

`src-examples` folder contains multiple example projects for quick start. For using the example just replace the `src` folder's content with your desired example project, and start development by running `gulp` command.

## Documentation

1. @todo: **[Installation](https://github.com/modularus/modularus/wiki)**
2. @todo: **[Folder structure](https://github.com/modularus/modularus/wiki)**
3. @todo: **[File types](https://github.com/modularus/modularus/wiki)**
4. @todo: **[Build configuration](https://github.com/modularus/modularus/wiki)**

### Folder Structure

The main application folder is `src` where are located all application source files. After you run `gulp` command it build your application (contactenates scripts, compiles LESS, assembles pages from partials etc), in `public` directory.

Main concept of modularus is hierarchical folder structure. Each folder corresponds to some specific feature in application and contains all it's specific files: scripts, templates, styles.

Though there is no any strict rule for file organization, we came up with following folder structure after working on multiple massive projects.

<!-- language:console -->

    public/
    src/
        .src-config/
        _assets/
        _common/
            common-module-1/
            common-module-2/
        _main/
        module-A/
            _common/
            module-a/
            module-b/
            module-c/
            module-A.html
            module-A.js
            module-A.css
        module-B/


`.src-config/` - here are located config files of how our application should be assembled. Also here is located bower.json file for 3rd party plugins and libraries.

`_assets/` - images, fonts

`_common/` - these folders contain the modules which are common for current hierarchy level. For example it can be `header/` or `footer/`. Note that all modules also can have some common features which is great to store in that module's `_common/` folder.

`_main` - here we can store main files of application: router, configuration, main layout, main style file and so on.

All other folders correspond to modules, which can contain their specific files and submodules.


### File types

Each module can contain following type of files

1. **scripts**
2. **styles**
3. **layouts** - layouts corespond to single HTML page in `public` folder when compiled. For single-page applications with AngularJS usually we have just a single "index" layout in `_main` folder
4. **templates** - parts of views, from which are assembled the layouts (header, footer, sidebar etc).

### Build configuration

Now that we have a nice folder structure, we should build our application. 
Modularus runs on gulp and comes with some basic tasks like `concat`, `rename`, `uglify`, `less` etc. If you want you can install other gulp plugins and define them in gulpfile.js.

The build configuration is stored in `.src-config` folder. We have `bower.json` where we define which external plugins to install, `paths-app.js` which describes how to build application files and `path-vendor.js` which describes how to handle vendor files (usually installed by bower).



## Authors

[Gevorg Harutyunyan](https://www.linkedin.com/profile/view?id=250051464)

[Aram Manukyan](https://www.linkedin.com/profile/view?id=278824620)
