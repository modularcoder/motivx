motivX.main

.config(function (webAPIAuthProvider, Config) {

	webAPIAuthProvider.setAPIUrl(Config.API.url); //Only requests to this endpoint will get the Authorization headers modified

	webAPIAuthProvider.setTokenEndpointUrl(Config.API.baseUrl + '/token');

	webAPIAuthProvider.setExternalUserInfoEndpointUrl(Config.API.url + '/api/account/externalUserInfo');

	webAPIAuthProvider.setRegisterExternalUserEndpointUrl(Config.API.url + '/api/account/registerExternal');

})

;