motivX.auth

.directive('authSocialButtons', function($log) {
	
	// Return directive configuration.
	return({
		templateUrl: 'templates/auth/_common/auth-social-buttons/auth-social-buttons.html',
		restrict: 'E',
		scope: false
	});

});