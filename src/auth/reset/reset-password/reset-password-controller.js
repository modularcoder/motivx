motivX.auth

.controller("resetPassCtrl", resetPassCtrl);

function resetPassCtrl($log, $state, $stateParams, Config, webAPIAuth, authService) {
	var vm = this;

	vm.resetError=false;
	vm.token = $stateParams.token;
    vm.email = $stateParams.email;

    
    if(!vm.token || !vm.email)
        $state.go('error', {code:404});

	vm.resetPass = function(){
		vm.user.emailAddress = vm.email;
		vm.user.passwordToken = vm.token;

		authService.resetPassword(vm.user).then(function(r){
			// Go back to login
			$state.go('auth.login');

		})
		.catch(function (error) {
			vm.resetError = true;
		});
	}
}
