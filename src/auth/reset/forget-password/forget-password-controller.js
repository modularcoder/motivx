motivX.auth

.controller("forgetPassCtrl", forgetPassCtrl);

function forgetPassCtrl($log, $state, Config, webAPIAuth, authService) {
	var vm = this;

	vm.forgetError = false;
	vm.forgetSuccess = false;

	vm.forgetPass = function() {

		authService.forgotPassword(vm.email)
		.then(function(){
			vm.forgetSuccess = true;
		})
		.catch(function (error) {
			vm.forgetError = true;
			vm.errormsg = error.data.message
		});;

	}
}
