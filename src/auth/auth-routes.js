motivX.auth

.config(function($stateProvider) {

	$stateProvider

	.state("auth", {
		abstract: true,
		templateUrl: "templates/auth/auth.html",
		controller: "AuthCtrl as auth",
		data: {},
		resolve : {
			loginProviders : function(authSocialButtonsService){
				return  authSocialButtonsService.get();
			}
		}
	})
	
	.state('auth.login', {
		url: "/login",
		templateUrl: "templates/auth/login/login.html",
		controller: "LoginCtrl as login",
		data: {
			disabledAfterLogin: true
		}
	})

	.state('auth.signup', {
		url: "/signup",
		templateUrl: "templates/auth/signup/signup.html",
		controller: "SignupCtrl as signup",
		data: {
			disabledAfterLogin: true
		}
	})

	.state('auth.forgetpass', {
		url: "/forgetpass",
		templateUrl: "templates/auth/reset/forget-password/forget-password.html",
		controller: "forgetPassCtrl as forgetPass",
		data: {
			disabledAfterLogin: true
		}
	})

	.state('auth.reset', {
		url: "/account/reset?token&email",
		templateUrl: "templates/auth/reset/reset-password/reset-password.html",
		controller: "resetPassCtrl as resetPass",
		data: {
			disabledAfterLogin: true
		}
	})

	.state('auth.logout', {
		url: "/logout",
		templateUrl: "templates/auth/logout/logout.html",
		controller: "LogoutCtrl as logout",
		data: {}
	})

	;


})

;