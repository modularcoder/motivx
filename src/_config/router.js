motivX.main

.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.when('', '/login');

	$urlRouterProvider.when('/', '/login');

	$urlRouterProvider.otherwise('/error?code=404');

})

.run(function ($rootScope, $state, $log, Config, webAPIAuth) {

	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {

		// If the route requires login, and user is not logged in
		// Redirect to login
		if(
			angular.isDefined(toState.data) &&
			toState.data.requiresLogin &&
			!webAPIAuth.isLoggedIn()
		) {
			$state.go("auth.login");
			event.preventDefault();
		}


		// If  the route is disabled after login, and user is logged in
		// Redirect to app default state
		if (
			angular.isDefined(toState.data) &&
			toState.data.disabledAfterLogin &&
			webAPIAuth.isLoggedIn()
		) {
			$state.go(Config.APP.defaultState);
			event.preventDefault();
		}
   
	});


})

;