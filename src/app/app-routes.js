motivX.app

.config(function($stateProvider) {

	$stateProvider

	.state('app', {
		abstract : true,
		templateUrl: "templates/app/app.html",
		controller: "AppCtrl as app",
		data: {
			requiresLogin: true  // This state requires login... (see _config/router.js)
		}
	})

	.state('app.rewards', {
		url: "/app/rewards",
		templateUrl: "templates/app/rewards/rewards.html",
		controller: "RewardsCtrl as rewards"
	})

	;

});