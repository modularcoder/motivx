motivX.app

.directive('headerMotivx', function($log, $document) {

	// Return directive configuration.
	return({
		templateUrl: 'templates/app/_common/header/header.html',
		restrict: 'E'
	});

});