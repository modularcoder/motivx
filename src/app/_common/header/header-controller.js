motivX.app

.controller("HeaderCtrl", HeaderCtrl);

function HeaderCtrl($log) {
	var vm = this;

	vm.navbarItems = [
		{
			title: 'Dashboard',
			action: 'dashboard'
		},
		{
			title: 'Rewards',
			action: 'coupons'
		},
		{
			title: 'Challenges',
			action: 'challenges'
		},
		{
			title: 'Devices & apps',
			action: 'app.account.devices'
		}
	];

	vm.accountNavItems = [
		{
			title: 'Settings',
			action: 'app.account.profile.config'
		},
		{
			title: 'History',
			action: 'app.account.history'
		},
		{
			title: 'Help',
			action: 'help'
		},
		{
			title: 'Sign out',
			action: 'app.account.signout'
		}
	];

	$log.log("Header Controller");
}
