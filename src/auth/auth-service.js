motivX.auth

.factory('authService', function($log, $http, Config) {

	return {
		register: function(user) {
			return $http.post(Config.API.url + "/api/account/register", user);
		},
		forgotPassword: function(email) {
      		return $http.post(Config.API.url + "/api/account/forgotPassword/" + email);
		},
		resetPassword: function(params) {
      		return $http.post(Config.API.url + "/api/account/ResetPassword", params);
		}
	}
});
