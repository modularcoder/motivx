motivX.auth

.controller("LoginCtrl", LoginCtrl);

function LoginCtrl($log, $state, $stateParams, Config, webAPIAuth) {
	var vm = this;

	vm.submitted = false;
	vm.remember = true;
	
	// Logging user in
	vm.login = function() {
		vm.submitted = true;

		webAPIAuth
		.login('password', vm.username, vm.password)
		.then(function () {
			$state.go(Config.APP.defaultState);
		})
		.catch(function () {
			vm.loginError = true;
		});
	}

}
