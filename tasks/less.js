var	less = require("gulp-less"); 
var config = require("../config");

module.exports = {
	runner: less,
	defaults: {
		paths: [ 
			config.src_dir,
			config.bower_dir,
		]
	}
};