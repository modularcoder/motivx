motivX.main

.constant('Config', {
	environment: 'production', //development or production
	API: {
		protocol: 'https', //window.location.protocol.split(':')[0], //Use the same protocol, host and port as the UI is hosted from bu default
		host: 'pilot-api.motivx.com',
		port: String(443),
		path: 'api',
		url: 'https://pilot-api.motivx.com:443', // just hardcode, will change later
		baseUrl: 'https://pilot-api.motivx.com'
	},
	APP: {
		defaultState: 'app.rewards',
		protocol: window.location.protocol.split(':')[0], //Use the same protocol, host and port as the UI is hosted from bu default
		host: window.location.host,
	}
})

;