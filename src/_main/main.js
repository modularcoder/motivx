var motivX = {};

motivX.main = angular.module("motivX", [

	// Vendor modules
	'ui.router',
	'ngStorage',
	'kennethlynne.webAPI2Authentication',
	'angular-loading-bar',

	// Application modules
	"motivX.app",
	"motivX.auth"
]);

// App module
motivX.app = angular.module("motivX.app", []);

// Auth module
motivX.auth = angular.module("motivX.auth", []);
